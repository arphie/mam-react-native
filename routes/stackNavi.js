import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import Dashboard from '../screens/Dashboard';
import Tiles from '../screens/Tiles';
import Profile from '../screens/Profile';
import Registration from '../screens/Registration';

import Login from '../screens/Login';

import RegisterName from '../screens/Register/Name';
import RegisterAccount from '../screens/Register/Account';
import RegisterPersonal from '../screens/Register/Personal';
import RegisterQrScanner from '../screens/Register/QrScanner';

const screens = {
    Dashboard: {
        screen: Dashboard
    },
    Login: {
        screen: Login
    },
    RegisterName: {
        screen: RegisterName
    },
    RegisterAccount: {
        screen: RegisterAccount
    },
    RegisterPersonal: {
        screen: RegisterPersonal
    },
    RegisterQrScanner: {
        screen: RegisterQrScanner
    },
    Dashboard: {
        screen: Dashboard
    },
    
    Profile: {
        screen: Registration
    },
    // Profile: {
    //     screen: Profile
    // },
    
    Tiles: {
        screen: Tiles
    },
    
}

const StackNavi = createStackNavigator(screens, {
    defaultNavigationOptions: {
        title: '',
        headerShown: false
    }
});

export default createAppContainer(StackNavi);