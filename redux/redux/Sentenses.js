
const initialState = {
    word:  [],
}

export default  function Sentenses(state = initialState, action) {
    switch (action.type) {
        case 'ADD_WORD':
            return { ...state, word: [...state.word, action.payload]}

        case 'RESET_WORD':    
            return { ...state, word: []}

        default:
            return state
    }
}