

export const addWord = (word) => {
    return {
        type: "ADD_WORD",
        payload: { word: word}
    }
}

export const resetWord = () => {
    return {
        type: "RESET_WORD",
        payload: { word: []}
    }
}