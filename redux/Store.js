import { createStore } from 'redux';
import mainReducer from './redux/Index';

export default createStore(mainReducer);