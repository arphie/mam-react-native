import React, { useState } from 'react';
import {
    Alert,
    StyleSheet,
    Text,
    View,
    Image,
    ScrollView,
    TouchableOpacity,
    TextInput,
    Button,
    Pressable,
    ImageBackground,
    Dimensions
} from 'react-native';
import { color } from 'react-native/Libraries/Components/View/ReactNativeStyleAttributes';
import { SafeAreaView} from 'react-navigation'
import { connect } from 'react-redux';

class Account extends React.Component {
    constructor(){
		super();
		this.state = {
            selectedName: '',
			email: '',
            isPasswordError: false,
            password: '',
            confirmPassword: ''
		}
	}

    getSetEmail(text){
        this.setState({email: text});
    }

    getSetPassword(text){
        this.setState({password: text});
    }

    getSetConfirmPassword(text){
        this.setState({confirmPassword: text});
    }

    processBack(){
        this.props.navigation.goBack();
    }

    processAccount(){
        this.setState({isPasswordError: false});
        if(this.state.password !== this.state.confirmPassword){
            this.setState({isPasswordError: true});
            return;
        }

        let userInfo = {
            'name': this.props.navigation.getParam('name'),
            'email': this.state.email,
            'password': this.state.password
        }

        console.log('go for gold -> ', userInfo);

        this.props.navigation.navigate('RegisterPersonal', userInfo)

        // 
    }


    render(){
        return (
            <SafeAreaView style={styles.mainContainer}>
                <ScrollView>
                    <ImageBackground style={ styles.imgBackground } resizeMode='cover' source={require('./../../assets/images/astro.png')}>
                        <View style={styles.framePlaceHolder}>
                            <Text style={styles.welcomeUser}>Hi! {this.props.navigation.getParam('name')}</Text>
                            <Text style={styles.subWelcomeUser}>Let's set you up right now</Text>
                            <View style={styles.accoutSetup}>
                                <TextInput style={styles.formInput} onChangeText={(e) => this.getSetEmail(e)} placeholder='Email' placeholderTextColor="#989898"  />
                                <TextInput secureTextEntry={this.state.password.length === 0? false : true} onChangeText={(e) => this.getSetPassword(e)} style={styles.formInput} placeholder='Password' placeholderTextColor="#989898"  />
                                <TextInput secureTextEntry={this.state.confirmPassword.length === 0? false : true} onChangeText={(e) => this.getSetConfirmPassword(e)} style={styles.formInput} placeholder='Confirm Password' placeholderTextColor="#989898"  />
                            </View>
                            <View style={[styles.passwordError, (this.state.isPasswordError ? styles.showPasswordErorr : '')]} >
                                <Text style={styles.passwordErrorMain}>Password did not match</Text>
                                <Text style={styles.passwordErrorSub}>Please Try Again</Text>
                            </View>
                            <Pressable style={styles.createButton} onPress={() => this.processAccount()}>
                                <Text style={styles.createButtonText}>Create Account</Text>
                            </Pressable>
                            <Pressable style={styles.backButton} onPress={() => this.processBack()}>
                                <Text style={styles.backButtonText}>Back</Text>
                            </Pressable>
                        </View>
                    </ImageBackground>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        width: '100%',
        height: '100%',
        flex: 1
    },
	imgBackground: {
        width: null,
        height: Dimensions.get('window').height,
        flex: 1 
    },
    framePlaceHolder: {
        backgroundColor: '#2b2b2b',
        height: '100%',
        opacity: .9,
        paddingVertical: '25%'
    },
    centerInput: {
        marginTop: '50%',
        marginBottom: '50%'
    },
    formInput: {
        textAlign: 'center',
        marginHorizontal: '20%',
        borderBottomWidth: 1,
        borderBottomColor: '#989898',
        color: '#fff',
        fontFamily: 'PTSansNarrow-Regular',
    },
    welcomeUser: {
        fontFamily: 'PTSansNarrow-Regular',
        fontSize: 28,
        color: '#fff',
        textAlign: 'center',
    },
    subWelcomeUser: {
        fontFamily: 'PTSansNarrow-Regular',
        fontSize: 16,
        color: '#fff',
        textAlign: 'center',
        marginBottom: 40
    },
    createButton: {
        backgroundColor: '#e9e367',
        marginHorizontal: '30%',
        marginTop: '20%',
        marginBottom: '5%',
        borderRadius: 50,
    },
    createButtonText: {
        textAlign: 'center',
        fontFamily: 'PTSansNarrow-Bold',
        fontSize: 15,
        paddingVertical: 10,
        // color: '#e9e367'
    },
    backButton: {
        marginBottom: '30%',
        marginHorizontal: '30%',
    },
    backButtonText: {
        textAlign: 'center',
        fontFamily: 'PTSansNarrow-Bold',
        fontSize: 15,
        paddingVertical: 10,
        color: '#e9e367'
    },
    passwordPlaceholderStyle: {
        fontFamily: 'PTSansNarrow-Regular',
        fontSize: 14
    },
    showPasswordErorr: {
        transform: [{ scale: 1 }]
    },
    passwordError: {
        marginHorizontal: '20%',
        marginTop: 20,
        backgroundColor: '#ffa2a2',
        paddingVertical: 5,
        paddingHorizontal: 15,
        borderRadius: 5,
        transform: [{ scale: 0 }]
    },
    passwordErrorMain: {
        color: '#fff',
        fontFamily: 'PTSansNarrow-Bold',
        fontSize: 16,
        textAlign: 'center'
    },
    passwordErrorSub: {
        color: '#fff',
        fontFamily: 'PTSansNarrow-Bold',
        fontSize: 16,
        textAlign: 'center'
    }
});

export default (Account);