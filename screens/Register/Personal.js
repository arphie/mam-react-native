import React, { useState } from 'react';
import {
    Alert,
    StyleSheet,
    Text,
    View,
    Image,
    ScrollView,
    TouchableOpacity,
    TextInput,
    Button,
    Pressable,
    ImageBackground,
    Dimensions
} from 'react-native';
import { color } from 'react-native/Libraries/Components/View/ReactNativeStyleAttributes';
import { SafeAreaView } from 'react-navigation'
import DatePicker from 'react-native-date-picker'
import { connect } from 'react-redux';

class Personal extends React.Component {
    constructor(){
		super();
		this.state = {
            name: '',
            email: '',
            password: '',
			firstName: '',
            lastName: '',
            middleName: '',
            birthdate: new Date(),
            address: '',
            school: '',
            contactNumber: '',
            openDateModal: true
		}
	}

    getSetFirstName(text){
        this.setState({firstName: text});
    }

    getSetLastName(text){
        this.setState({lastName: text});
    }

    getSetMiddleName(text){
        this.setState({middleName: text});
    }

    getSetAddress(text){
        this.setState({address: text});
    }

    getSetSchool(text){
        this.setState({school: text});
    }

    getSetContactNumber(text){
        this.setState({contactNumber: text});
    }

    updateBirthdate(date){
        this.setState({birthdate: date});

    }

    processBack(){
        this.props.navigation.goBack();
    }
    
    processProfile(){
        // console.log(this.state);
        this.props.navigation.navigate('RegisterQrScanner', this.state)

    }

    componentDidMount(){
        this.setState({middleName: this.props.navigation.getParam('name')});
        this.setState({email: this.props.navigation.getParam('email')});
        this.setState({password: this.props.navigation.getParam('password')});
    }


    render(){
        return (
            <SafeAreaView style={styles.mainContainer}>
                <ScrollView>
                    <ImageBackground style={ styles.imgBackground } resizeMode='cover' source={require('./../../assets/images/astro.png')}>
                        <View style={styles.framePlaceHolder}>
                            <Text style={styles.welcomeUser}>Profile Information</Text>
                            <Text style={styles.subWelcomeUser}>Let us know who you are</Text>
                            <View style={styles.accoutSetup}>
                                <TextInput style={styles.formInput} onChangeText={(e) => this.getSetFirstName(e)} placeholder='First Name' placeholderTextColor="#989898"  />
                                <TextInput style={styles.formInput} onChangeText={(e) => this.getSetLastName(e)} placeholder='Last Name' placeholderTextColor="#989898"  />
                                {/* <TextInput style={styles.formInput} onChangeText={(e) => this.getSetMiddleName(e)} placeholder='Middle Name' placeholderTextColor="#989898"  /> */}

                                <View style={styles.birthdate}>
                                    <Text style={styles.birthdateTitle}>Birthdate</Text>
                                    {/* <Button title="Set Birthdate" onPress={() => this.openDate()} /> */}
                                    <DatePicker style={styles.birthdatePicker} title="Birthdate" theme="light" mode="date" date={this.state.birthdate} onDateChange={(date) => this.updateBirthdate(date)} androidVariant="nativeAndroid" textColor="#fff" />
                                </View>

                                <TextInput style={styles.formInput} onChangeText={(e) => this.getSetAddress(e)} placeholder='Address' placeholderTextColor="#989898"  />
                                <TextInput style={styles.formInput} onChangeText={(e) => this.getSetSchool(e)} placeholder='School' placeholderTextColor="#989898"  />
                                <TextInput style={styles.formInput} onChangeText={(e) => this.getSetContactNumber(e)} placeholder='Contact Number' placeholderTextColor="#989898"  />
                                
                            </View>
                            <Pressable style={styles.createButton} onPress={() => this.processProfile()}>
                                <Text style={styles.createButtonText}>Save profile information</Text>
                            </Pressable>

                            <Pressable style={styles.backButton} onPress={() => this.processBack()}>
                                <Text style={styles.backButtonText}>Back</Text>
                            </Pressable>
                        </View>
                    </ImageBackground>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        width: '100%',
        height: '100%',
        flex: 1
    },
	imgBackground: {
        width: null,
        height: Dimensions.get('window').height + 100,
        // flex: 1 
    },
    birthdate: {
        // marginHorizontal: '12%'
    }, 
    birthdateTitle: {
        textAlign: 'center',
        color: '#fff',
        fontFamily: 'PTSansNarrow-Regular',
    },
    birthdatePicker: {
        marginHorizontal: '12%',
        // fontFamily: 'PTSansNarrow-Regular',
    },
    framePlaceHolder: {
        backgroundColor: '#2b2b2b',
        height: '100%',
        opacity: .9,
        paddingVertical: '16%'
    },
    centerInput: {
        marginTop: '50%',
        marginBottom: '50%'
    },
    formInput: {
        textAlign: 'center',
        marginHorizontal: '20%',
        borderBottomWidth: 1,
        borderBottomColor: '#989898',
        color: '#fff',
        fontFamily: 'PTSansNarrow-Regular',
        marginBottom: 15
    },
    welcomeUser: {
        fontFamily: 'PTSansNarrow-Regular',
        fontSize: 28,
        color: '#fff',
        textAlign: 'center',
    },
    subWelcomeUser: {
        fontFamily: 'PTSansNarrow-Regular',
        fontSize: 16,
        color: '#fff',
        textAlign: 'center',
        marginBottom: 40
    },
    createButton: {
        backgroundColor: '#e9e367',
        marginHorizontal: '30%',
        marginTop: '20%',
        borderRadius: 50,
    },
    createButtonText: {
        textAlign: 'center',
        fontFamily: 'PTSansNarrow-Bold',
        fontSize: 15,
        paddingVertical: 10,
        // color: '#e9e367'
    },
    passwordPlaceholderStyle: {
        fontFamily: 'PTSansNarrow-Regular',
        fontSize: 14
    },
    backButton: {
        marginBottom: '0%',
        marginHorizontal: '30%',
    },
    backButtonText: {
        textAlign: 'center',
        fontFamily: 'PTSansNarrow-Bold',
        fontSize: 15,
        paddingVertical: 10,
        color: '#e9e367'
    },
});

export default (Personal);