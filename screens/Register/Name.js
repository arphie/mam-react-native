import React, { useState } from 'react';
import {
    Alert,
    StyleSheet,
    Text,
    View,
    Image,
    ScrollView,
    TouchableOpacity,
    TextInput,
    Button,
    Pressable,
    ImageBackground,
    Dimensions
} from 'react-native';
import { color } from 'react-native/Libraries/Components/View/ReactNativeStyleAttributes';
import { SafeAreaView, createStackNavigator } from 'react-navigation'
import { connect } from 'react-redux';



class Name extends React.Component {
    constructor(){
		super();
		this.state = {
			name: ''
		}
        

	}

    getName(e){
        this.setState({name: e});
    }

    processName(){
        console.log('name -> ', this.state.name);
        // console.warn('test');
        this.props.navigation.navigate('RegisterAccount', {name: this.state.name})
    }

    processToLogin(){
        this.props.navigation.navigate('Login')
    }

    render(){
        return (
            <SafeAreaView style={styles.mainContainer}>
                <ScrollView>
                    <ImageBackground style={ styles.imgBackground } resizeMode='cover' source={require('./../../assets/images/astro.png')}>
                        <View style={styles.framePlaceHolder}>
                            <View style={styles.centerInput}>
                                <Text style={styles.mainText}>My name is</Text>
                                <Text style={styles.subWelcomeUser}>Let's us know how you want to be called</Text>
                                <TextInput style={styles.formInput} onChangeText={(e) => this.getName(e)} placeholder='Pick your name' placeholderTextColor="#989898"  />
                            </View>
                            <Pressable style={styles.createButton} onPress={() => this.processName()}>
                                <Text style={styles.createButtonText}>Let's get started!</Text>
                            </Pressable>
                            <Pressable style={styles.loginButton} onPress={() => this.processToLogin()}>
                                <Text style={styles.loginButtonText}>Login to Account</Text>
                            </Pressable>
                        </View>
                    </ImageBackground>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        width: null,
        height: '100%',
        flex: 1
    },
	imgBackground: {
        width: null,
        height: Dimensions.get('window').height,
        flex: 1 
    },
    framePlaceHolder: {
        backgroundColor: '#2b2b2b',
        height: '100%',
        opacity: .8
    },
    centerInput: {
        marginTop: '50%',
        marginBottom: '30%'
    },
    formInput: {
        textAlign: 'center',
        marginHorizontal: 40,
        borderBottomWidth: 1,
        borderBottomColor: '#989898',
        color: '#fff',
        fontFamily: 'PTSansNarrow-Regular',
    },
    mainText: {
        fontFamily: 'PTSansNarrow-Regular',
        fontSize: 28,
        color: '#fff',
        textAlign: 'center'
    },
    createButton: {
        backgroundColor: '#e9e367',
        marginHorizontal: '30%',
        marginBottom: '5%',
        borderRadius: 50,
    },
    createButtonText: {
        textAlign: 'center',
        fontFamily: 'PTSansNarrow-Regular',
        fontSize: 15,
        paddingVertical: 10,
        
    },
    loginButton: {
        marginHorizontal: '30%',
        marginBottom: '40%',
        borderRadius: 50,
    },
    loginButtonText: {
        textAlign: 'center',
        fontFamily: 'PTSansNarrow-Bold',
        fontSize: 15,
        paddingVertical: 10,
        color: '#e9e367'
    },
    subWelcomeUser: {
        fontFamily: 'PTSansNarrow-Regular',
        fontSize: 16,
        color: '#fff',
        textAlign: 'center',
        marginBottom: 40
    }
});

export default (Name);