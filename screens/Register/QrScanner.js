import React, { useState } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    ScrollView,
    TouchableOpacity,
    TextInput,
    Button,
    Pressable,
    ImageBackground,
    Dimensions,
    Linking,
    PermissionsAndroid
} from 'react-native';

import { SafeAreaView, createStackNavigator } from 'react-navigation'

// import { QRreader } from "react-native-qr-decode-image-camera";
import RNQRGenerator from 'rn-qr-generator';


import * as ImagePicker from "react-native-image-picker"
import {launchImageLibrary, launchCamera} from 'react-native-image-picker'

import { connect } from 'react-redux';
import { RNCamera } from 'react-native-camera';

import axios from 'axios';

class QrScanner extends React.Component {
    constructor(){
		super();
		this.state = {
			email: '',
            password: '',
			firstName: '',
            lastName: '',
            middleName: '',
            birthdate: '',
            address: '',
            school: '',
            contactNumber: '',
            signUpOptions: '',
            qrCode: '',
            teacherInfo: null,
            showGetTeacherEror: false,
            filePath: '',
            QrValue: null
		}


	}

    onSuccess(e){
        console.log(e);
        console.log('has scanned');
        // this.scanner.reactivate();

    }

    reactivateScanner(){
        console.log('scanner has reactivated');
        this.scanner.reactivate()
    }

    loadStateData(){
        console.log('get states -> ', this.state);
    }

    setTeacherCode(text){
        this.setState({qrCode: text});
    }

    processQrCode(){
        console.log('qr code -> ', this.state.qrCode);
        let self = this;
        const data = {
            teacher: this.state.qrCode
        }

        console.log('teacher -> ', data);

        axios.get('https://pecsboard.com/api/teacher/info', {params: data})
        .then(function (response) {
            let responseData = response.data.data;
            console.log('response data ->', responseData);
            self.setState({teacherInfo: responseData});
        })
        .catch((error) => {
            console.log('error ->', JSON.stringify(error.message));
            this.setState({showGetTeacherEror: true});
        });
    }

    proceedWithRegistration(){
        
        let self = this;
        let student_information = {
            teacher: this.state.teacherInfo.id,
            first_name: this.state.firstName,
            last_name: this.state.lastName,
            middle_name: this.state.middleName,
            email: this.state.email,
            birth_date: this.state.birthdate,
            address: this.state.address,
            school: this.state.school,
            contact_number: this.state.contactNumber,
            password: this.state.password
        }
        console.log('process now -> ', student_information);

        axios.post('https://pecsboard.com/api/teacher/request', student_information)
        .then(function (response) {
          let responseData = response;
          self.setState({signUpOptions: 'thank_you'});
        })
        .catch((error) => {
          console.log('tracking error ', JSON.stringify(error.message));
        //   resetSentense();
        });

        // console.log('student to save ->', student_information);
        
        
        // 
    }

    resetItems(){
        console.log('go resest');
        this.setState({qrCode: ''});
        this.setState({signUpOptions: ''});
        this.setState({teacherInfo: null});
        this.setState({showGetTeacherEror: false});
    }

    goBackToLogin(){
        console.log('back to login');
        this.props.navigation.navigate('Login')
    }

    changeOption(type){
        this.setState({signUpOptions: type});
    }

    getQRData(image){
        let self = this;
        RNQRGenerator.detect(image).then((response) => {
            const { values } = response; // Array of detected QR code values. Empty if nothing found.
            console.log('has values for QR code ->', response.values[0]);
            self.setState({qrCode: response.values[0]});
        }).catch(error => console.log('Cannot detect QR code in image', error));

    }

    chooseFile(type){
        let options = {
            mediaType: type,
            maxWidth: 300,
            maxHeight: 550,
            quality: 1,
            includeBase64: true
        };
        launchImageLibrary(options, (response) => {
            // console.log('Response = ', response);
            
            if (response.didCancel) {
                alert('User cancelled camera picker');
                return;
            } else if (response.errorCode == 'camera_unavailable') {
                alert('Camera not available on device');
                return;
            } else if (response.errorCode == 'permission') {
                alert('Permission not satisfied');
                return;
            } else if (response.errorCode == 'others') {
                alert(response.errorMessage);
                return;
            }
            // console.log('base64 -> ', response.assets[0].base64);
            console.log('path -> ', response.assets[0].path);
            console.log('uri -> ', response.assets[0].uri);
            console.log('width -> ', response.assets[0].width);
            console.log('height -> ', response.assets[0].height);
            console.log('fileSize -> ', response.assets[0].fileSize);
            console.log('type -> ', response.assets[0].type);
            console.log('fileName -> ', response.assets[0].fileName);
            this.setState({filePath: response.assets[0]});
            
            
            this.getQRData({ uri: response.assets[0].uri });
        
        });
    }

    captureImage = async (type) => {
        let options = {
          mediaType: type,
          maxWidth: 300,
          maxHeight: 550,
          quality: 1,
          videoQuality: 'low',
          durationLimit: 30, //Video max duration in seconds
          saveToPhotos: true,
          includeBase64: true
        };
        let isCameraPermitted = await this.requestCameraPermission();
        let isStoragePermitted = await this.requestExternalWritePermission();
        if (isCameraPermitted && isStoragePermitted) {
          launchCamera(options, (response) => {
            console.log('Response = ', response);
    
            if (response.didCancel) {
            //   alert('User cancelled camera picker');
              return;
            } else if (response.errorCode == 'camera_unavailable') {
              alert('Camera not available on device');
              return;
            } else if (response.errorCode == 'permission') {
              alert('Permission not satisfied');
              return;
            } else if (response.errorCode == 'others') {
              alert(response.errorMessage);
              return;
            }

            let respo = response.assets[0];
            console.log('get respobase -> ', respo);

            // console.log('base64 -> ', response.assets[0].base64);
            console.log('uri -> ', response.assets[0].uri);
            console.log('width -> ', response.assets[0].width);
            console.log('height -> ', response.assets[0].height);
            console.log('fileSize -> ', response.assets[0].fileSize);
            console.log('type -> ', response.assets[0].type);
            console.log('fileName -> ', response.assets[0].fileName);
            this.setState({filePath: response.assets[0]});

            this.getQRData({ base64: response.assets[0].base64 });

          });
        }
      };

    requestCameraPermission = async () => {
        if (Platform.OS === 'android') {
            try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA,
                {
                title: 'Camera Permission',
                message: 'App needs camera permission',
                },
            );
            // If CAMERA Permission is granted
            return granted === PermissionsAndroid.RESULTS.GRANTED;
            } catch (err) {
            console.warn(err);
            return false;
            }
        } else return true;
    };

    requestExternalWritePermission = async () => {
        if (Platform.OS === 'android') {
          try {
            const granted = await PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
              {
                title: 'External Storage Write Permission',
                message: 'App needs write permission',
              },
            );
            // If WRITE_EXTERNAL_STORAGE Permission is granted
            return granted === PermissionsAndroid.RESULTS.GRANTED;
          } catch (err) {
            console.warn(err);
            alert('Write permission err', err);
          }
          return false;
        } else return true;
      };

    componentDidMount(){
        this.setState({email: this.props.navigation.getParam('email')});
        this.setState({password: this.props.navigation.getParam('password')});
        this.setState({firstName: this.props.navigation.getParam('firstName')});
        this.setState({lastName: this.props.navigation.getParam('lastName')});
        this.setState({middleName: this.props.navigation.getParam('middleName')});
        this.setState({birthdate: this.props.navigation.getParam('birthdate')});
        this.setState({address: this.props.navigation.getParam('address')});
        this.setState({school: this.props.navigation.getParam('school')});
        this.setState({contactNumber: this.props.navigation.getParam('contactNumber')});
        // console.log('email -> ', this.props.navigation.getParam('email'));
    }

    render(){
        return (
            <SafeAreaView style={styles.container}>
				<ScrollView style={styles.scrollView}>
                    <ImageBackground style={ styles.imgBackground } resizeMode='cover' source={require('./../../assets/images/astro.png')}>
                        <View style={styles.qrContainer}>
                            {
                                ( this.state.signUpOptions == '' ?
                                    <View style={styles.dashBased}>
                                        <TouchableOpacity style={styles.teacherPlaceholder} onPress={() => this.changeOption('enter_qr_code')}>
                                            <Text style={styles.teacherCode}>Enter Teacher Code</Text>
                                        </TouchableOpacity>
                                        <View>
                                            <Text style={styles.orPlace}>or</Text>
                                        </View>
                                        <TouchableOpacity style={styles.teacherQRPlaceholder} onPress={() => this.changeOption('scan_qr_code')}>
                                            <Text style={styles.teacherQRCode}>Scan Teachers QR Code</Text>
                                        </TouchableOpacity>
                                    </View>
                                :
                                    <View></View>
                                )
                            }

                            {
                                ( this.state.signUpOptions == 'enter_qr_code' ?
                                    <View style={styles.qrCodeEnter}>
                                        {
                                            (this.state.teacherInfo !== null ? 
                                                <View>
                                                    <View style={styles.teacherImageContainer}>
                                                        <View style={styles.teacherImageContainer}><Image source={{uri: ''+this.state.teacherInfo.profile_avatar+''}} style={styles.tileTeacherImage} /></View>
                                                        <Text style={styles.teacherInfoName}>{this.state.teacherInfo.first_name} {this.state.teacherInfo.last_name}</Text>
                                                        <Text style={styles.teacherInfoEmail}>{this.state.teacherInfo.email}</Text>
                                                        <Text style={styles.getQRCode}>{this.state.teacherInfo.dteachertoken}</Text>
                                                    </View>
                                                    <TouchableOpacity style={styles.teacherPlaceholder} onPress={() => this.proceedWithRegistration()}>
                                                        <Text style={styles.teacherCode}>Confirm Teacher</Text>
                                                    </TouchableOpacity>
                                                    <TouchableOpacity style={styles.qrCodeScanBackConts} onPress={() => this.resetItems()}>
                                                        <Text style={styles.qrCodeScanBack}>Back</Text>
                                                    </TouchableOpacity>
                                                </View>
                                            :
                                                <View>
                                                    {
                                                        (this.state.showGetTeacherEror ? 
                                                            <View>
                                                                <Text style={styles.noTeacher}>Teacher not found</Text>
                                                                <TouchableOpacity style={styles.teacherPlaceholder} onPress={() => this.resetItems()}>
                                                                    <Text style={styles.teacherCode}>Try again</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                        :
                                                            <View>
                                                                <TextInput style={styles.formInput} onChangeText={(e) => this.setTeacherCode(e)} placeholder='Teacher QR Code' placeholderTextColor="#989898"  />
                                                                <TouchableOpacity style={styles.teacherPlaceholder} onPress={() => this.processQrCode()}>
                                                                    <Text style={styles.teacherCode}>Check Teacher Code</Text>
                                                                </TouchableOpacity>
                                                                <TouchableOpacity style={styles.qrCodeScanBackConts} onPress={() => this.resetItems()}>
                                                                    <Text style={styles.qrCodeScanBack}>Back</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                        )
                                                    }
                                                    
                                                </View>
                                            )
                                        }
                                    </View>
                                :
                                    ( this.state.signUpOptions == 'scan_qr_code' ? 
                                        <View>
                                            <View style={styles.container}>
                                                <View>
                                                    <Text style={styles.useQRCode}>Request for teacher instructions</Text>
                                                </View>
                                                {
                                                    (this.state.filePath != '' ? 
                                                        <View>
                                                            
                                                            
                                                            {
                                                                (this.state.qrCode == '' ?

                                                                    <View style={styles.qrErrorsContainer}>
                                                                        <Image source={{uri: this.state.filePath.uri}} style={styles.QrCodeImage} />
                                                                        <Text style={styles.qrStateError}>Something went wrong with the image.</Text>
                                                                        <Text style={styles.qrStateError}>Failed to get QR code Data</Text>
                                                                        <Text style={styles.qrStateError}>Please try again</Text>
                                                                    </View>
                                                                :
                                                                    ( this.state.teacherInfo !== null ? 
                                                                        <View>
                                                                            <View style={styles.teacherImageContainer}>
                                                                                <View style={styles.teacherImageContainer}><Image source={{uri: ''+this.state.teacherInfo.profile_avatar+''}} style={styles.tileTeacherImage} /></View>
                                                                                <Text style={styles.teacherInfoName}>{this.state.teacherInfo.first_name} {this.state.teacherInfo.last_name}</Text>
                                                                                <Text style={styles.teacherInfoEmail}>{this.state.teacherInfo.email}</Text>
                                                                                <Text style={styles.getQRCode}>{this.state.teacherInfo.dteachertoken}</Text>
                                                                            </View>
                                                                            <TouchableOpacity style={styles.teacherPlaceholder} onPress={() => this.proceedWithRegistration()}>
                                                                                <Text style={styles.teacherCode}>Confirm Teacher</Text>
                                                                            </TouchableOpacity>
                                                                            <TouchableOpacity style={styles.qrCodeScanBackConts} onPress={() => this.resetItems()}>
                                                                                <Text style={styles.qrCodeScanBack}>Back</Text>
                                                                            </TouchableOpacity>
                                                                        </View>
                                                                    :
                                                                        <View>
                                                                            <Image source={{uri: this.state.filePath.uri}} style={styles.QrCodeImage} />
                                                                            <Text style={styles.getQRCode}>{this.state.qrCode}</Text>
                                                                            <TouchableOpacity style={styles.qrCodeScanBackOptionsConts} onPress={() => this.processQrCode()}>
                                                                                <Text style={styles.qrCodeScanOptionsBack}>Proceed</Text>
                                                                            </TouchableOpacity>
                                                                        </View>
                                                                    )
                                                                    
                                                                    
                                                                )
                                                            }
                                                        </View>
                                                    :
                                                        // <View style={styles.qrImagePlaceholder}></View>
                                                        <View>
                                                            <View>
                                                                {
                                                                    (this.state.teacherInfo !== null ? 
                                                                        <View>
                                                                            <View style={styles.teacherImageContainer}><Image source={{uri: ''+this.state.teacherInfo.profile_avatar+''}} style={styles.tileTeacherImage} /></View>
                                                                            <Text style={styles.teacherInfoName}>{this.state.teacherInfo.first_name} {this.state.teacherInfo.last_name}</Text>
                                                                            <Text style={styles.teacherInfoEmail}>{this.state.teacherInfo.email}</Text>
                                                                            <Text style={styles.getQRCode}>{this.state.teacherInfo.dteachertoken}</Text>
                                                                            <TouchableOpacity style={styles.qrCodeScanBackOptionsConts} onPress={() => this.proceedWithRegistration()}>
                                                                                <Text style={styles.qrCodeScanOptionsBack}>Confirm Teacher</Text>
                                                                            </TouchableOpacity>
                                                                        </View>
                                                                    : 
                                                                        <View>
                                                                            <View style={styles.instructionPlaceholder}>
                                                                                <Text style={styles.qrInstructions}>either take a photo of the QR code</Text>
                                                                                <Text style={styles.qrInstructions}>or you can upload from gallery</Text>
                                                                            </View>
                                                                            <View style={styles.iconContainer}>
                                                                                <View style={styles.leftPart}>
                                                                                    <TouchableOpacity activeOpacity={0.5} style={styles.buttonStyle} onPress={() => this.captureImage('photo')}>
                                                                                        <Image source={require('./../../assets/images/camera.png')} style={styles.imageStyle} />
                                                                                    </TouchableOpacity>
                                                                                </View>
                                                                                <View style={styles.rightPart}>
                                                                                    <TouchableOpacity activeOpacity={0.5} style={styles.buttonStyle} onPress={() => this.chooseFile('photo')}>
                                                                                        <Image source={require('./../../assets/images/gallery.png')} style={styles.imageStyle} />
                                                                                    </TouchableOpacity>
                                                                                </View>
                                                                            </View>
                                                                        </View>
                                                                    )
                                                                }
                                                                
                                                                
                                                                
                                                            </View>
                                                            <TouchableOpacity style={styles.qrCodeScanBackOptionsConts} onPress={() => this.resetItems()}>
                                                                <Text style={styles.qrCodeScanOptionsBack}>Back</Text>
                                                            </TouchableOpacity>
                                                        </View>
                                                    )
                                                }
                                                
                                            </View>
                                        </View>
                                    :
                                        <View></View>
                                    )
                                )
                            }

                            {
                                ( this.state.signUpOptions == 'qr_scan' ?
                                    <View style={styles.qrCodeScan}>
                                        <TouchableOpacity style={styles.teacherPlaceholder}>
                                            <Text style={styles.teacherCode}>Enter Teacher Code</Text>
                                        </TouchableOpacity>
                                        <View>
                                            <Text style={styles.orPlace}>or</Text>
                                        </View>
                                        <TouchableOpacity style={styles.teacherQRPlaceholder}>
                                            <Text style={styles.teacherQRCode}>Scan Teachers QR Code</Text>
                                        </TouchableOpacity>
                                    </View>
                                :
                                    <View></View>
                                )
                            }

                            {
                                ( this.state.signUpOptions == 'thank_you' ?
                                    <View style={styles.thankYouPlace}>
                                        <View style={styles.thanksMessageContainer}>
                                            <Text style={styles.mainThanks}>Hi {this.props.navigation.getParam('name')}!</Text>
                                            <Text style={styles.mainCongratsThanks}>Congratulations!</Text>
                                            <Text style={styles.subThanks}>You have created your account</Text>
                                            <Text style={styles.subThanks}>Please wait for your teacher to </Text>
                                            <Text style={styles.subThanks}>approve your application.</Text>
                                        </View>
                                        <TouchableOpacity style={styles.teacherQRPlaceholder} onPress={() => this.goBackToLogin()}>
                                            <Text style={styles.teacherQRCode}>Login to Account now</Text>
                                        </TouchableOpacity>
                                    </View>
                                :
                                    <View></View>
                                )
                            }
                            
                        </View>
                    </ImageBackground>
                </ScrollView>
			</SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    iconContainer: {
        flexDirection: "row",
        flexWrap: 'wrap',
    },
    leftPart: {
        width: '49%',
        alignItems: 'flex-end'
    },
    rightPart: {
        width: '49%',
    },
    imageStyle: {
        width: 100,
        height: 100,
        margin: 5,
    },
    qrImagePlaceholder: {
        height: 90
    },
    QrCodeImage: {
        width: '60%',
        height: 400,
        marginHorizontal: '19%',
        marginBottom: '10%'
    },
    instructionPlaceholder: {
        marginBottom: 20
    },  
    useQRCode: {
        color: '#e9e367',
        fontFamily: 'PTSansNarrow-Bold',
        textAlign: 'center',
        fontSize: 26,
        marginBottom: 50
    },
    teacherInfoEmail: {
        color: '#e9e367',
        fontFamily: 'PTSansNarrow-Bold',
        textAlign: 'center',
        fontSize: 16,
        marginBottom: 10
    },
    teacherInfoName: {
        color: '#e9e367',
        fontFamily: 'PTSansNarrow-Bold',
        textAlign: 'center',
        fontSize: 16,
        marginBottom: 0
    },
    qrErrorsContainer: {
        backgroundColor: 'red',
        marginHorizontal: '20%',
        marginBottom: 20,
        padding: 15,
        borderRadius: 10,
    },
    qrStateError: {
        color: '#fff',
        textAlign: 'center'
    },
    getQRCode: {
        color: '#e9e367',
        fontFamily: 'PTSansNarrow-Bold',
        textAlign: 'center',
        fontSize: 24
    },
    noTeacher: {
        color: '#e9e367',
        fontFamily: 'PTSansNarrow-Bold',
        textAlign: 'center',
        fontSize: 38,
        marginBottom: 90
    },
    qrInstructions: {
        color: '#e9e367',
        fontFamily: 'PTSansNarrow-Bold',
        textAlign: 'center',
        fontSize: 16
    },
    thankYouPlace: {
        marginVertical: '40%'
    },
    thanksMessageContainer: {
        marginBottom: 100
    },
    mainCongratsThanks: {
        color: '#e9e367',
        fontFamily: 'PTSansNarrow-Bold',
        textAlign: 'center',
        fontSize: 48,
        marginBottom: 20
    },
    mainThanks: {
        color: '#e9e367',
        fontFamily: 'PTSansNarrow-Bold',
        textAlign: 'center',
        fontSize: 38,
        marginBottom: 0
    },
    subThanks: {
        color: '#e9e367',
        fontFamily: 'PTSansNarrow-Normal',
        fontSize: 18,
        textAlign: 'center',
    },  
    teacherImageContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 10
    },
    tileImage: {
        width: 150,
        height: 150,
        
        marginVertical: '0%',
        borderRadius: 150,
        borderWidth: 5,
        borderColor: '#e9e367',
        backgroundColor: '#FFF'
    },
    tileTeacherImage: {
        width: 150,
        height: 150,
        marginHorizontal: '32%',
        marginBottom: 10,
        borderRadius: 150,
        borderWidth: 5,
        borderColor: '#e9e367',
        backgroundColor: '#FFF'
    },
    mainTeacherName: {
        textAlign: 'center',
        color: '#e9e367',
        fontFamily: 'PTSansNarrow-Bold',
        marginVertical: '5%',
        fontSize: 21
    },
    imgBackground:{
        width: null,
        height: Dimensions.get('window').height + 100,
    },
    cameraContainer: {
        height: Dimensions.get('window').height - 200,
    },
    whiteText: {
        color: '#fff'
    },  
	container: {
		// flex: 1,
		backgroundColor: '#2b2b2b',
		// paddingTop: 20,
		// paddingHorizontal: 20,
		fontFamily: 'PTSansNarrow-Regular'
	},
	scrollView: {
		height: Dimensions.get('window').height,
		// marginHorizontal: 20,
	},
    dashBased: {
        marginVertical: '50%'
    },  
    qrContainer: {
        // marginVertical: '10%'
        // height: Dimensions.get('window').height,
        backgroundColor: '#2b2b2b',
        height: '100%',
        opacity: .9,
        paddingVertical: '16%'
    },
    buttonText: {
        color: '#fff',
        position: 'relative'
    },
    buttonTouchable: {
        // position: 'absolute',
        // zIndex: 9,
        // bottom: 20
    },
    qrCodeEnter:{
        marginVertical: '30%'
    },
    formInput: {
        textAlign: 'center',
        marginHorizontal: '20%',
        borderBottomWidth: 1,
        borderBottomColor: '#989898',
        color: '#fff',
        fontFamily: 'PTSansNarrow-Regular',
        marginBottom: '20%',
        fontSize: 19
    },
    orPlace: {
        textAlign: 'center',
        color: '#e9e367',
        fontFamily: 'PTSansNarrow-Bold',
        marginVertical: '5%',
        fontSize: 21
    },
    teacherPlaceholder: {
        backgroundColor: '#e9e367',
        marginHorizontal: '20%',
        borderRadius: 50,
    },
    qrCodeScanBackOptionsConts: {
        backgroundColor: '#e9e367',
        marginHorizontal: '20%',
        borderRadius: 50,
        marginTop: 25
    },
    teacherCode: {
        textAlign: 'center',
        fontFamily: 'PTSansNarrow-Bold',
        fontSize: 18,
        paddingVertical: 15,
    },
    teacherQRPlaceholder: {
        backgroundColor: '#e9e367',
        marginHorizontal: '20%',
        borderRadius: 50,
    },
    teacherQRCode: {
        textAlign: 'center',
        fontFamily: 'PTSansNarrow-Bold',
        fontSize: 18,
        paddingVertical: 15,
    },
    qrCodeScanBackConts: {
        marginBottom: '0%',
        marginHorizontal: '30%',
        marginTop: 30
    },
    qrCodeScanBack: {
        textAlign: 'center',
        fontFamily: 'PTSansNarrow-Bold',
        fontSize: 15,
        paddingVertical: 10,
        color: '#e9e367'
    },
    qrCodeScanOptionsBack: {
        textAlign: 'center',
        fontFamily: 'PTSansNarrow-Bold',
        fontSize: 15,
        paddingVertical: 10,
        color: '#000',
    },
    textStyle: {
        padding: 10,
        fontSize: 14,
        color: '#fff',
    },
    buttonStyle: {
        alignItems: 'center',
        flexDirection: 'row',
        // backgroundColor: '#DDDDDD',
        padding: 5,
    },
    
});

// AppRegistry.registerComponent('default', () => QrScanner);


export default (QrScanner);