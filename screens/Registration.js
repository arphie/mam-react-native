import React, { useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  TouchableOpacity,
  TextInput,
  Button,
  Pressable,
  ActivityIndicator
} from 'react-native';
import { SafeAreaView } from 'react-navigation'
import { connect } from 'react-redux';

import axios from 'axios';

class Registration extends React.Component {
	constructor(){
		super();
		this.state = {
			user_id: '',
			first_name: '',
			last_name: '',
			middle_name: '',
			birth_date: '',
			address: '',
			school: '',
			contact_number: '',
			password: '',
			confirmpassword: '',
			showPassError: false,
			showSuccess: false,
			showLoading: false,
			UserSchema: {
                name: "Users",
                properties: {
                    _id: "int",
                    student_id: "int",
                    first_name: "string",
                    last_name: "string",
                    middle_name: "string",
                    email: "string",
                    birth_date: "string",
                    address: "string",
                    school: "string",
                    contact_number: "string",
                },
                primaryKey: "_id",
            }
		}
	}

	getUserInformation(){
		console.log('get user information');

		( async () => {
			let self = this;
	
			const realm = await Realm.open({
			  path: "mammobile",
			  schema: [this.state.UserSchema],
			  deleteRealmIfMigrationNeeded: true,
			});
			const info = realm.objects("Users");
			console.log('informatuion -> ', info);
			if(info.length > 0){
				this.setState({user_id: info[0].student_id});
				this.setState({first_name: info[0].first_name});
				this.setState({last_name: info[0].last_name});
				this.setState({middle_name: info[0].middle_name});
				this.setState({birth_date: info[0].birth_date});
				this.setState({address: info[0].address});
				this.setState({school: info[0].school});
				this.setState({contact_number: info[0].contact_number});

				this.setState({email: info[0].email});
			} else {
				this.props.navigation.navigate('Dashboard')
			}

			
			
			realm.close();

			// this.props.navigation.navigate('Dashboard')

	
		})();
	}

	backToDashboard(){
		this.props.navigation.navigate('Dashboard')
	}

	logoutNow(){
		console.log('logout now!');

		( async () => {
			let self = this;
	
			const realm = await Realm.open({ path: "mammobile", schema: [this.state.UserSchema], deleteRealmIfMigrationNeeded: true, });

			const info = realm.objects("Users");

			realm.write(() => {
				realm.delete(info);
			})

			this.props.navigation.navigate('Login')
			
			realm.close();
	
		})();
	}

	getSetFirstName(text){
        this.setState({first_name: text});
    }

	getSetLastName(text){
        this.setState({last_name: text});
    }

	getSetNickName(text){
        this.setState({middle_name: text});
    }

	getSetAddress(text){
        this.setState({address: text});
    }

	getSetSchool(text){
        this.setState({school: text});
    }

	getSetContact(text){
        this.setState({contact_number: text});
    }

	getSetPassword(text){
        this.setState({password: text});
    }

	getSetConfirmPassword(text){
        this.setState({confirmpassword: text});
    }

	updateInfo(){
		this.setState({showPassError: false});
		let newUserInfo = {
			"student": this.state.user_id,
			"first_name": this.state.first_name,
			"last_name": this.state.last_name,
			"middle_name": this.state.middle_name,
			"address": this.state.address,
			"school": this.state.school,
			"contact_number": this.state.contact_number
		};
		console.log('newUserInfo -> ', newUserInfo);
		// check password
		if(this.state.password != ""){
			console.log('this opass');
			if(this.state.password != this.state.confirmpassword){
				this.setState({showPassError: true});
			} else {
				newUserInfo.password = this.state.password;
			}
		}

		this.setState({showLoading: true});

        let self = this;
		axios.post('https://pecsboard.com/api/student/info/update', newUserInfo, {timeout: 10000})
        .then((response) => {
            let userInfo = response.data;
            console.log('login data -> ', userInfo.data);
			self.setState({password: ''});
			self.setState({confirmpassword: ''});
			self.setState({showSuccess: true});
			self.setState({showLoading: false});
            ( async () => {
				let self = this;
		
				const realm = await Realm.open({
				  path: "mammobile",
				  schema: [this.state.UserSchema],
				  deleteRealmIfMigrationNeeded: true,
				});
				const info = realm.objects("Users");
				
				realm.write(() => {
					realm.delete(info);
					realm.create("Users", {
						_id: 1,
						student_id: userInfo.data.id,
						first_name: userInfo.data.first_name,
						last_name: userInfo.data.last_name,
						middle_name: userInfo.data.middle_name,
						email: userInfo.data.email,
						birth_date: userInfo.data.birth_date,
						address: userInfo.data.address,
						school: userInfo.data.school,
						contact_number: userInfo.data.contact_number,
					});
				})

				realm.close();

		
			})();
        })
        .catch((error) => {
			console.log(error);
			self.setState({showLoading: false});
        });
	}

	componentDidMount(){
		this.getUserInformation();
	}

	render(){
		return (
			<SafeAreaView style={styles.container}>
				<ScrollView style={styles.scrollView}>
					<View style={styles.header}>
						<View style={styles.mainColHead}>
							<View style={styles.mainColLeft}>
								<Text style={styles.user_name}>Hi {this.state.first_name}!</Text>
								<Text style={styles.hello_header}>All about you</Text>
							</View>
							<View style={styles.mainColRight}>
								<TouchableOpacity style={styles.signoutcontainer} onPressIn={() => this.logoutNow()}>
									{/* <Image style={styles.isUserImage} source={require('./../assets/images/user.png')} /> */}
									<Text style={styles.signout}>Sign out</Text>
								</TouchableOpacity>
							</View>
						</View>
					</View>
					<View style={styles.formsContainer}>
						<View style={styles.formsInner}>
							

							{
								( this.state.showLoading ?
									<ActivityIndicator size="large" color="#e9e367" />
								:	
									<View>
										{(this.state.showSuccess ? 
											<View style={styles.successContainer}>
												<Text style={styles.passwordSuccess}>Information Successfully Updated</Text>
											</View>
										: <View></View> )}
										{(this.state.showPassError ? 
											<View>
												<Text style={styles.passwordError}>password and confirm password did not match</Text>
											</View>	
										: <View></View> )}
									</View>
								)
								
							}
							
							<View style={styles.formSeparator}>
								<Text style={styles.formSeparatorText}>Student Information</Text>
							</View>
							<View style={styles.formItem}>
								<Text style={styles.formLabel}>First Name</Text>
								<TextInput style={styles.formInput} onChangeText={(e) => this.getSetFirstName(e)} defaultValue={this.state.first_name} placeholder='John Peter' />
							</View>
							<View style={styles.formItem}>
								<Text style={styles.formLabel}>Last Name</Text>
								<TextInput style={styles.formInput} onChangeText={(e) => this.getSetLastName(e)} defaultValue={this.state.last_name} placeholder='Doe' />
							</View>
							<View style={styles.formItem}>
								<Text style={styles.formLabel}>NickName</Text>
								<TextInput style={styles.formInput} onChangeText={(e) => this.getSetNickName(e)} defaultValue={this.state.middle_name} placeholder='Sunshine' />
							</View>
							<View style={styles.formItem}>
								<Text style={styles.formLabel}>Address</Text>
								<TextInput style={styles.formInput} onChangeText={(e) => this.getSetAddress(e)} defaultValue={this.state.address} placeholder='Street Name/House Number/Barangay/City' />
							</View>
							<View style={styles.formItem}>
								<Text style={styles.formLabel}>School</Text>
								<TextInput style={styles.formInput} onChangeText={(e) => this.getSetSchool(e)} defaultValue={this.state.school} placeholder='Sample School' />
							</View>
							<View style={styles.formItem}>
								<Text style={styles.formLabel}>Contact number</Text>
								<TextInput style={styles.formInput} onChangeText={(e) => this.getSetContact(e)} defaultValue={this.state.contact_number} placeholder='(---)--- ----' />
							</View>
							<View style={styles.formSeparator}>
								<Text style={styles.formSeparatorText}>Account Information</Text>
							</View>
							
							
							<View style={styles.formItem}>
								<Text style={styles.formLabel}>Email</Text>
								<TextInput style={styles.formInput} defaultValue={this.state.email} placeholder='testing@sample.com' editable = {false}/>
							</View>
							<View style={styles.formItem}>
								<Text style={styles.formLabel}>Password</Text>
								<TextInput secureTextEntry={true} type="password" onChangeText={(e) => this.getSetPassword(e)} defaultValue={this.state.password} style={styles.formInput} placeholder='***********' />
							</View>
							<View style={styles.formItem}>
								<Text style={styles.formLabel}>Confirm Password</Text>
								<TextInput secureTextEntry={true} type="password" onChangeText={(e) => this.getSetConfirmPassword(e)} defaultValue={this.state.confirmpassword} style={styles.formInput} placeholder='***********' />
							</View>
							<View style={styles.formItem}>
								<View style={styles.OptionContainer}>
									<View style={styles.dUpdateLeftPart}>
										<Pressable style={styles.processButton} onPress={() => this.backToDashboard()} >
											<Text style={styles.processButtonText}>Back</Text>
										</Pressable>
									</View>
									<View style={styles.dUpdateRightPart}>
										<Pressable style={styles.processButton} onPress={() => this.updateInfo()} >
											<Text style={styles.processButtonText}>Update Information</Text>
										</Pressable>
									</View>
								</View>
							</View>
						</View>
					</View>
				</ScrollView>
			</SafeAreaView>
		);
	}
}


const styles = StyleSheet.create({
	header: {
		width: '100%',
		height: 80,
		padding: 20,
		marginTop: 60,
		justifyContent: 'center',
	},
	mainColHead: {
		flexDirection: "row",
		flexWrap: 'wrap',
	},
	mainColLeft: {
		width: '77%'
	},
	mainColRight: {
		width: '20%',
	},
	OptionContainer: {
		flexDirection: "row",
		flexWrap: 'wrap',
	},
	dUpdateLeftPart: {
		width: '47%',
		marginRight: '3%'
	},
	dUpdateRightPart: {
		width: '49%',
	},
	hello_header: {
		fontSize: 18,
		color: "#fff",
		fontFamily: 'PTSansNarrow-Bold',
	},
	user_name: {
		fontSize: 26,
		fontWeight: '600',
		color: '#e9e367',
		fontFamily: 'PTSansNarrow-Bold',
	},
	isUserImage: {
		width: 39,
		height: 39,
		// position: 'absolute',
		// right: 0
	},
	container: {
		// flex: 1,
		backgroundColor: '#2b2b2b',
		paddingTop: 20,
		paddingHorizontal: 20,
		fontFamily: 'PTSansNarrow-Regular'
	},
	scrollView: {
		
		// marginHorizontal: 20,
	},
	formsContainer: {
		// flex: 1
	},
	formSeparator: {
		marginBottom: 10
	},
	formSeparatorText: {
		color: '#e9e367',
		fontFamily: 'PTSansNarrow-Bold',
		fontSize: 18
	},
	formsInner: {
		padding: 20
	},	
	formItem: {

	},
	formLabel: {
		fontSize: 16,
		color: '#98a0af',
		fontFamily: 'PTSansNarrow-Regular',
		// fontWeight: 'bold'
	},
	passwordError: {
		fontSize: 16,
		color: 'red',
		fontFamily: 'PTSansNarrow-Regular',
		backgroundColor: 'pink',
		paddingVertical: 10,
		paddingHorizontal: 20,
		marginBottom: 15,
		borderRadius: 8
	},
	passwordSuccess: {
		fontSize: 16,
		color: '#000',
		fontFamily: 'PTSansNarrow-Regular',
		backgroundColor: '#e9e367',
		paddingVertical: 10,
		paddingHorizontal: 20,
		marginBottom: 15,
		borderRadius: 8
	},
	formInput: {
		fontFamily: 'PTSansNarrow-Regular',
		backgroundColor: '#fff',
		// elevation: 10,
    	shadowColor: '#222',
		fontSize: 18,
		borderRadius: 3,
		paddingHorizontal: 15,
		paddingVertical: 5,
		width: '100%',
		marginTop: 5,
		marginBottom: 25,
		color: '#000'
	},
	processButton: {
		backgroundColor: '#e9e367',
		borderRadius: 5
	},
	processButtonText: {
		color: '#000',
		textAlign: 'center',
		paddingVertical: 10,
		textTransform: 'uppercase',
		fontFamily: 'PTSansNarrow-Bold'
	},
	signoutcontainer: {
		backgroundColor: '#e9e367',
		borderRadius: 5
	},
	signout: {
		color: '#000',
		textAlign: 'center',
		paddingVertical: 10,
		// textTransform: 'uppercase',
		fontFamily: 'PTSansNarrow-Bold'
	}
//   iconInner: {
//     // flex: 1,
//     height: 120,
//     alignItems: 'center',
//     justifyContent: 'center',
//     backgroundColor: 'pink'
//   },

});

// const mapStateToProps = (state, wordProps) => {
//   return {
//     words: state.Sentenses.word
//   }
// }

// const mapDispatchToProps = (dispatch) => {
//   return {
//     addWord: (word) => dispatch(addWord(word)),
//     resetWord: () => dispatch(resetWord())
//   }
// }

export default (Registration);
