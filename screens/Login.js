import React, { useState } from 'react';
import {
    Alert,
    StyleSheet,
    Text,
    View,
    Image,
    ScrollView,
    TouchableOpacity,
    TextInput,
    Button,
    Pressable,
    ImageBackground,
    Dimensions
} from 'react-native';
import { color } from 'react-native/Libraries/Components/View/ReactNativeStyleAttributes';
import { SafeAreaView, createStackNavigator } from 'react-navigation'
import { connect } from 'react-redux';

import axios from 'axios';

class Login extends React.Component {
    constructor(){
		super();
		this.state = {
			email: '',
            password: '',
            isLoginLoading: false,
            isLoginErrorShow: false,
            isLoginTimeOut: false,
            UserSchema: {
                name: "Users",
                properties: {
                    _id: "int",
                    student_id: "int",
                    first_name: "string",
                    last_name: "string",
                    middle_name: "string",
                    email: "string",
                    birth_date: "string",
                    address: "string",
                    school: "string",
                    contact_number: "string",
                },
                primaryKey: "_id",
            }
		}
        

	}

    getEmail(e){
        this.setState({email: e});
    }

    getPassword(e){
        this.setState({password: e});
    }

    processRegister(){
        this.props.navigation.navigate('RegisterName')
    }

    processLogin(){
          
        console.log('login email -> ', this.state.email);
        console.log('login password -> ', this.state.password);

        const loginData = {
            "email": this.state.email,
	        "password": this.state.password
        }
        
        let self = this;
        this.setState({isLoginLoading: true});
        this.setState({isLoginTimeOut: false});
        this.setState({isLoginErrorShow: false});

        axios.post('https://pecsboard.com/api/login', loginData, {timeout: 10000})
        .then((response) => {
            this.setState({isLoginLoading: false});
            let userInfo = response.data;
            console.log('login data -> ', userInfo.data);
            
            if(Object.keys(userInfo.data).length > 0){
                console.log('login success -> ', userInfo.data);

                ( async () => {
                    let self = this;
            
                    const realm = await Realm.open({
                      path: "mammobile",
                      schema: [this.state.UserSchema],
                      deleteRealmIfMigrationNeeded: true,
                    });
                    const info = realm.objects("Users");
                    
                    realm.write(() => {
                        realm.delete(info);
                        realm.create("Users", {
                            _id: 1,
                            student_id: userInfo.data.id,
                            first_name: userInfo.data.first_name,
                            last_name: userInfo.data.last_name,
                            middle_name: userInfo.data.middle_name,
                            email: userInfo.data.email,
                            birth_date: userInfo.data.birth_date,
                            address: userInfo.data.address,
                            school: userInfo.data.school,
                            contact_number: userInfo.data.contact_number,
                        });
                    })

                    realm.close();

                    this.props.navigation.navigate('Dashboard')

            
                })();


            } else {
                console.log('login wrong');
                this.setState({isLoginErrorShow: true});
            }
        })
        .catch((error) => {
            if(error.message.includes('Network Error') || error.message.includes('timeout')){
                this.setState({isLoginTimeOut: true});
                this.setState({isLoginLoading: false});
            }
        });
        


    }
    

    render(){
        return (
            <SafeAreaView style={styles.mainContainer}>
                <ScrollView>
                    <ImageBackground style={ styles.imgBackground } resizeMode='cover' source={require('./../assets/images/astro.png')}>
                        <View style={styles.framePlaceHolder}>
                            <View style={styles.centerInput}>
                                <Text style={styles.mainText}>Login now</Text>
                                <Text style={styles.subWelcomeUser}>and start your journey</Text>
                                <TextInput style={styles.formInput} onChangeText={(e) => this.getEmail(e)} placeholder='Email Address' placeholderTextColor="#989898"  />
                                <TextInput secureTextEntry={this.state.password.length === 0? false : true} onChangeText={(e) => this.getPassword(e)} style={styles.formInput} placeholder='Password' placeholderTextColor="#989898"  />
                                <View style={styles.errorContent}>
                                    {(this.state.isLoginLoading ? <Text style={styles.ifLoginLoading}>Loggin in....</Text> : <Text></Text> )}
                                    {(this.state.isLoginErrorShow ? <Text style={styles.ifLoginError}>We did not found your account</Text> : <Text></Text> )}
                                    {(this.state.isLoginTimeOut ? <Text style={styles.ifLoginError}>Something wrong with your connection{"\n"}Please try again later</Text> : <Text></Text> )}
                                </View>
                            </View>
                            <Pressable style={styles.createButton} onPress={() => this.processLogin()}>
                                <Text style={styles.createButtonText}>Login</Text>
                            </Pressable>
                            <Pressable style={styles.backButton} onPress={() => this.processRegister()}>
                                <Text style={styles.backButtonText}>Register</Text>
                            </Pressable>
                            
                        </View>
                    </ImageBackground>
                </ScrollView>
            </SafeAreaView>
        )
    }


}

const styles = StyleSheet.create({
    errorContent: {
        marginTop: 20,
    },
    ifLoginLoading:{
        textAlign: 'center',
        fontFamily: 'PTSansNarrow-Bold',
        fontSize: 16,
        color: 'green'
    },
    ifLoginError: {
        textAlign: 'center',
        fontFamily: 'PTSansNarrow-Bold',
        fontSize: 16,
        color: 'red'
    },
    mainContainer: {
        width: null,
        height: null,
        flex: 1
    },
	imgBackground: {
        width: null,
        height: Dimensions.get('window').height,
        flex: 1 
    },
    formInput: {
        textAlign: 'center',
        marginHorizontal: '20%',
        borderBottomWidth: 1,
        borderBottomColor: '#989898',
        color: '#fff',
        fontFamily: 'PTSansNarrow-Regular',
        fontSize: 18
    },
    centerInput: {
        marginTop: '30%',
        marginBottom: '30%'
    },
    mainText: {
        fontFamily: 'PTSansNarrow-Regular',
        fontSize: 32,
        color: '#fff',
        textAlign: 'center'
    },
    subWelcomeUser: {
        fontFamily: 'PTSansNarrow-Regular',
        fontSize: 24,
        color: '#fff',
        textAlign: 'center',
        marginBottom: 40
    },
    framePlaceHolder: {
        backgroundColor: '#2b2b2b',
        height: '100%',
        opacity: .8
    },
    createButton: {
        backgroundColor: '#e9e367',
        marginHorizontal: '30%',
        marginTop: '20%',
        marginBottom: '5%',
        borderRadius: 50,
    },
    createButtonText: {
        textAlign: 'center',
        fontFamily: 'PTSansNarrow-Bold',
        fontSize: 24,
        paddingVertical: 10,
        // color: '#e9e367'
    },
    backButton: {
        marginBottom: '30%',
        marginHorizontal: '30%',
    },
    backButtonText: {
        textAlign: 'center',
        fontFamily: 'PTSansNarrow-Bold',
        fontSize: 24,
        paddingVertical: 10,
        color: '#e9e367'
    },
});

export default (Login);