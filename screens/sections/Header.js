import React, { useState } from 'react';

import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';

const Header = (props) => {

  const goToProfile = (props) => {
    console.log("this is the mian -> ", );
    props.clickProfile(true);
  }

  return (
    <View style={styles.header}>
        <View style={styles.mainColHead}>
          <View style={styles.mainColLeft}>
            <Text style={styles.hello_header}>Good Morning</Text>
            <Text style={styles.user_name}>User</Text>
          </View>
          <View style={styles.mainColRight}>
            <TouchableOpacity onPress={() => goToProfile(true)}>
              <Text >User Info</Text>
            </TouchableOpacity>
          </View>
        </View>
    </View>
  );
}

const styles = StyleSheet.create({
    header: {
        width: '100%',
        height: 80,
        padding: 20,
        // backgroundColor: "#fff",
        // alignItems: 'center',
        justifyContent: 'center',
        // borderRadius: 10,
        // shadowColor: "#b9b9b9",
        // shadowOffset: {
        //     width: 0,
        //     height: 3,
        // },
        // shadowOpacity: 0.27,
        // shadowRadius: 4.65,
        // elevation: 6,
    },
    hello_header: {
      fontSize: 12
    },
    user_name: {
      fontSize: 18,
      fontWeight: '600'
    },
    mainColHead: {
      flexDirection: "row",
      flexWrap: 'wrap',
    },
    mainColLeft: {
      width: '77%'
    },
    mainColRight: {
      width: '20%',
      backgroundColor: 'pink'
    },
});


export default Header;
