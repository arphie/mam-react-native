import React, { useState } from 'react';

import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  TouchableOpacity,
  Button
} from 'react-native';
import { connect } from 'react-redux';

const Profile = ({ navigation, addWord, resetWord, ...props }) => {

  const iconSelected = (word) => {
    addWord(word);
    navigation.goBack();
  }

  return (
    <View>
        
          <View style={styles.header}>
            <View style={styles.mainColHead}>
              <View style={styles.mainColLeft}>
                <Button title="<-" onPress={() => navigation.goBack() } />
              </View>
              <View style={styles.mainColRight}>
                <Text style={styles.hello_header}>Good Morning</Text>
                <Text style={styles.user_name}>User</Text>
              </View>
            </View>
            {/* BOF user information */}
            <View>
              <Text>user information should go here</Text>
            </View>
          </View>
        {/* <Text>{ navigation.getParam('icons')}</Text> */}
    </View>
  );
}

const styles = StyleSheet.create({

  // BOF header
  header: {
    width: '100%',
    height: 80,
    padding: 20,
    // backgroundColor: "#fff",
    // alignItems: 'center',
    justifyContent: 'center',
    // borderRadius: 10,
    // shadowColor: "#b9b9b9",
    // shadowOffset: {
    //     width: 0,
    //     height: 3,
    // },
    // shadowOpacity: 0.27,
    // shadowRadius: 4.65,
    // elevation: 6,
  },
  hello_header: {
    fontSize: 12
  },
  user_name: {
    fontSize: 18,
    fontWeight: '600'
  },
  mainColHead: {
    flexDirection: "row",
    flexWrap: 'wrap',
  },
  mainColLeft: {
    width: '20%'
  },
  mainColRight: {
    width: '74%',
    marginLeft: '5%'
  },
  // EOF Header

  iconItemRow: {
    flexDirection: "row",
    flexWrap: 'wrap',
    width: '92%',
    margin: '4%',
    // padding: 20,
    // backgroundColor: "red",
    flex: 1
  },
  iconItem: {
    // flex: 1,
    width: '50%',
    height: 120,
    padding: 5,
    marginBottom: 10
  },
  iconInner: {
    // flex: 1,
    height: 120,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'pink'
  },

});

const mapStateToProps = (state, wordProps) => {
  return {
    words: state.Sentenses.word
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addWord: (word) => dispatch(addWord(word)),
    resetWord: () => dispatch(resetWord())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
