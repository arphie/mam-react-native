import React, { useState, useEffect } from 'react';

import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  TouchableOpacity,
  PermissionsAndroid,
  Button,
  Alert,
  FlatList,
  Platform
} from 'react-native';

import PageHeader from './../screens/sections/Header'

// Text to Speech
import Tts from 'react-native-tts';

import axios from 'axios';

// import add word
import { connect } from 'react-redux';
import { addWord, resetWord } from './../redux/Actions';

import { NavigationEvents } from 'react-navigation';

import Realm, {RealmConfiguration } from "realm";

const Dashboard = ({ navigation, addWord, resetWord, ...props }) => {
    const [category, setCategory] = useState([]);
    const [sid, setStudentID] = useState(0);
    const [sname, setStudentName] = useState('');

    const UserSchema = {
      name: "Users",
      properties: {
        _id: "int",
        student_id: "int",
        first_name: "string",
        last_name: "string",
        middle_name: "string",
        email: "string",
        birth_date: "string",
        address: "string",
        school: "string",
        contact_number: "string",
      },
      primaryKey: "_id",
    };

    const BoardSchema = {
      name: "board",
      properties: {
        _id: "int",
        tracking: "string",
        pecs_board: "string",
      },
      primaryKey: "_id",
    };

    const trackingSchema = {
      name: "tracking",
      properties: {
        _id: "string",
        student_id: "string",
        name: "string",
        category: "string",
        tile: "string",
        sentense: "string",
        iscorrect: "string",
        septrace: "string",
      },
      primaryKey: "_id",
    };
    
    const checkIfUserHasLogin = () => {
      // console.log('get info');
      ( async () => {
        let self = this;

        const realm = await Realm.open({ path: "mammobile", schema: [UserSchema], deleteRealmIfMigrationNeeded: true,});

        const info = realm.objects("Users");
        // console.log('student information -> ', info);
        
        if(info.length > 0){
          // get user id
          setStudentName(info[0].first_name);
          setStudentID(info[0].student_id);
          loadPecsBoard(info[0].student_id);
        } else {
          // console.log('has no user data');
          navigation.navigate('Login')
        }

        realm.close();

      })();
    }

    const loadPecsBoard = (studentid) => {
      // console.log('load new board from https://pecsboard.com/api/student/pecs/'+studentid);
      axios.get('https://pecsboard.com/api/student/pecs/'+studentid)
      .then(function (response) {
        let responseData = response.data.data;
        // console.log('get pecs -> ', responseData);

        // let rebuild = [];
        // responseData.forEach((item, i) => {
        //   let baseTiles = {
        //     heading: item.heading,
        //     id: item.id,
        //     icons: [],
        //     image: '',
        //   };
        //   console.log('icons head -> ', item.icons);
        //   item.icons.forEach((eitem, ei) => {
            
        //     let iconbased = {
        //       name: eitem.name,
        //       icon: '',
        //       word: eitem.word,
        //       id: eitem.id,
        //     };



        //     console.log('icons inner -> ', iconbased);
        //   });
        // });

        ( async () => {
            let self = this;
    
            const realm = await Realm.open({ path: "mammobile", schema: [BoardSchema], deleteRealmIfMigrationNeeded: true, });

            const info = realm.objects("board");
            
            realm.write(() => {
                realm.delete(info);
                realm.create("board", {
                    _id: 1,
                    tracking: new Date().toDateString(),
                    pecs_board: JSON.stringify(responseData)
                });

            })

            const newBoard = realm.objects("board");
            const dboard = JSON.parse(newBoard[0].pecs_board);
            // console.log('new boared -> ', dboard);
            setCategory(category => (dboard))

            realm.close();
        })();

      })
      .catch((error) => {
        console.log(JSON.stringify(error.message));

        ( async () => {
          let self = this;
  
          const realm = await Realm.open({
            path: "mammobile",
            schema: [BoardSchema],
            deleteRealmIfMigrationNeeded: true,
          });
          
          const newBoard = realm.objects("board");
          const dboard = JSON.parse(newBoard[0].pecs_board);
          console.log('new boared -> ', dboard);
          setCategory(category => (dboard))

          realm.close();
      })();
      });
    }

    const pressHandler = () => {
      navigation.navigate('Tiles', icons);
    }

    const processShowProfile = (event) =>{
      console.log('process profile now ->', event);
    }    

    const sanitizeSentense = (words) => {
      let dword = "";
      words.map(
        word => (
          dword = dword +" "+ word.word.name
        )
      )

      return dword;
    }

    const resetSentense = () => {
      console.log("Reset Sentenses");
      resetWord();
    }

    const readSentense = () => {
      // get sensense
      let dword = "";
      props.words.map(
        word => (
          dword = dword +" "+ word.word.name
        )
      )

      let wordbase = props.words;
      let tosubs = [];
      let dtimemap = new Date().getTime();
      wordbase.map((item, i) => {
          let mapping = {
            "_id": dtimemap+"-"+i,
            "student_id": ''+sid,
            "name": item.word.name,
            "category": "activities",
            "tile": item.word.icon,
            "sentense": dword,
            "iscorrect": "yes",
            "septrace": dtimemap+""
          };
          tosubs.push(mapping);
          // console.log(JSON.stringify(mapping));
        }
      );


      Tts.speak(sanitizeSentense(props.words), {
        latency: 100
      });

      Tts.stop();

      

      ( async () => {
        let self = this;

        const realm = await Realm.open({ path: "mammobile", schema: [trackingSchema], deleteRealmIfMigrationNeeded: true, });

        const info = realm.objects("tracking");
        
        realm.write(() => {
            tosubs.map((item, id) => {
              realm.create("tracking", item);
            });
        })

        const getTracking = realm.objects("tracking");
        // console.log('tracking info -> ', getTracking);
        const tiletosave = { "entry": getTracking }

        axios.post(` https://pecsboard.com/api/student/tracking`, tiletosave)
        .then(function (response) {
          let responseData = response;
          realm.write(() => {
            realm.delete(getTracking); 
          })
          
          realm.close();
          resetSentense();
        })
        .catch((error) => {
          console.log('tracking error ', JSON.stringify(error.message));
          resetSentense();
        });
        

        
      })();

      
      
      
    }

    const loadDetails = () => {
      console.log('has data loaded');
      checkIfUserHasLogin();

      
    }

    useEffect(() => {
      checkIfUserHasLogin();
      // checkPermission();
    }, []);
    

  return (
      <ScrollView style={styles.mainComponent}>
        <View>
          <NavigationEvents onDidFocus={() => loadDetails() } />
          <View style={styles.header}>
            <View style={styles.mainColHead}>
              <View style={styles.mainColLeft}>
                <Text style={styles.user_name}>Hi {sname}!</Text>
                <Text style={styles.hello_header}>Have a great day!</Text>
              </View>
              <View style={styles.mainColRight}>
                <TouchableOpacity onPressIn={() => navigation.navigate('Profile')}>
                  <Image style={styles.isUserImage} source={require('./../assets/images/user.png')} />
                  {/* <Text style={styles.userText}>User Info</Text> */}
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View>
            {/* <Button style={styles.buttonStyle} title="asdasdasdasd" onPress={pressHandler} /> */}
              <View style={styles.buttonSentenses}>
                <Text style={styles.sentensePhase}>{ sanitizeSentense(props.words) }</Text>
                <View style={styles.buttonPlaceholder}>
                  <Text style={styles.resetText} onPress={resetSentense}>Reset</Text>
                  <Text style={styles.sayText} onPress={readSentense}>Voice</Text>
                </View>
              </View>
              <View style={styles.tilesMainContainer}>
                <View style={styles.tilesContainer}>
                  {
                    ( category.length == 0 ? 
                      <View style={styles.noTilesContainer}>
                        <Text style={styles.noBase}>No tiles yet</Text>
                        <Text style={styles.noSubBase}>Please wait patiently for your teacher</Text>
                        <Text style={styles.noSubBase}>to add your tiles.</Text>
                      </View>
                    :
                      <View></View>
                    )
                  }
                  {category.map((item, i)=> (
                    <View key={i}  style={styles.iconItemRow}>
                      <TouchableOpacity onPress={() => navigation.navigate('Tiles', {icons: item.icons, name: item.heading}) }>
                        {/* <Image style={styles.tileImage} source={item.image} /> */}
                        <Image source={{uri: ''+item.image+''}} style={styles.tileImage} />
                        <Text style={styles.headerTitle}>{item.heading}</Text>
                      </TouchableOpacity>
                    </View>
                  ))}
                </View>
              </View>
          </View>
        </View>
      </ScrollView>
  );
}

const styles = StyleSheet.create({
  mainComponent: {
    flex: 1,
    backgroundColor: '#2b2b2b'
  },
  isUserImage: {
    width: 39,
    height: 39,
    // position: 'absolute',
    // right: 0
  },
  // BOF header
  header: {
      width: '100%',
      height: 80,
      padding: 20,
      marginTop: 60,
      // backgroundColor: "#fff",
      // alignItems: 'center',
      justifyContent: 'center',
      // borderRadius: 10,
      // shadowColor: "#b9b9b9",
      // shadowOffset: {
      //     width: 0,
      //     height: 3,
      // },
      // shadowOpacity: 0.27,
      // shadowRadius: 4.65,
      // elevation: 6,
  },
  noTilesContainer: {
    width: '100%'
  },
  noBase: {
    fontSize: 26,
    fontWeight: '600',
    color: '#e9e367',
    fontFamily: 'PTSansNarrow-Bold',
    textAlign: 'center',
    marginBottom: 20
  },
  noSubBase: {
    fontSize: 16,
    fontWeight: '600',
    color: '#e9e367',
    fontFamily: 'PTSansNarrow-Bold',
    textAlign: 'center',
  },
  userText: {
    color: "#fff"
  },
  hello_header: {
    fontSize: 18,
    color: "#fff",
    fontFamily: 'PTSansNarrow-Bold',
  },
  user_name: {
    fontSize: 26,
    fontWeight: '600',
    color: '#e9e367',
    fontFamily: 'PTSansNarrow-Bold',
  },
  mainColHead: {
    flexDirection: "row",
    flexWrap: 'wrap',
  },
  mainColLeft: {
    width: '77%'
  },
  mainColRight: {
    width: '20%',
  },
  // EOF Header
  tileImage: {
    width: 150,
    height: 150,
    marginHorizontal: '5%',
    marginVertical: '5%',
    borderRadius: 5,
    borderWidth: 3,
    borderColor: '#e9e367',
    backgroundColor: '#FFF'
  },
  sentensePhase: {
    borderWidth: 2,
    borderColor: '#ddd',
    backgroundColor: "#ddd",
    borderRadius: 6,
    width: '92%',
    paddingVertical: 15,
    paddingHorizontal: 15,
    // lineHeight: 16,
    fontSize: 16,
    marginBottom: 10,
    fontFamily: 'PTSansNarrow-Regular',
  },
  buttonSentenses: {
    width: '94%',
    margin: '6%',
  },
  buttonPlaceholder: {
    flexDirection: "row",
    flexWrap: 'wrap',
    width: '92%',
  },
  tilesMainContainer: {
    marginHorizontal: '7%',
    marginTop: 40
  },
  tilesContainer:{
    flexDirection: "row",
    flexWrap: 'wrap',
    width: '100%',
    margin: 0,
  },
  resetText: {
    width: '49%',
    marginRight: '1%',
    textAlign: 'center',
    backgroundColor: '#e9e367',
    fontSize: 18,
    padding: 10,
    fontFamily: 'PTSansNarrow-Bold',
    borderRadius: 5
  },
  sayText: {
    width: '50%',
    display: 'flex',
    textAlign: 'center',
    backgroundColor: '#e9e367',
    fontSize: 18,
    padding: 10,
    fontFamily: 'PTSansNarrow-Bold',
    borderRadius: 5
  },
  iconItemRow: {
    width: '48%',
    marginRight: '1%',
    marginLeft: '1%',
    marginBottom: '1%',
    borderRadius: 3,
    textAlign: 'center'
  },
  iconItem: {
    // flex: 1,
    width: '50%',
    height: 120,
    padding: 5,
    marginBottom: 10,
    display: 'flex',
    
  },
  iconInner: {
    // flex: 1,
    height: 120,
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerTitle: {
    borderRadius: 7,
    fontSize: 16,
    fontFamily: 'PTSansNarrow-Regular',
    paddingTop: 5,
    marginBottom: 10,
    textAlign: 'center',
    color: '#fff'
  }
});

const mapStateToProps = (state, wordProps) => {
  // console.log(JSON.stringify(state));
  return {
    words: state.Sentenses.word
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addWord: (word) => dispatch(addWord(word)),
    resetWord: () => dispatch(resetWord())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
