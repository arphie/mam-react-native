import React, { useState, useEffect } from 'react';

import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  TouchableOpacity,
  Button
} from 'react-native';
import { connect } from 'react-redux';
import { addWord, resetWord } from './../redux/Actions';

const Tiles = ({ navigation, addWord, resetWord, ...props }) => {
  const [tileIcons, setTileIcons] = useState([]);

  const iconSelected = (word) => {
    addWord(word);
    navigation.goBack();
  }

  const loadIconsHere = () => {
    console.log('load images here');
    setTileIcons(navigation.getParam('icons'));

    
  }

  const loadIcons = () => {


    return (
      <View style={styles.iconItemRow}>
        {
          (tileIcons !== null ? 
            tileIcons.map(icons=> (
              <View key={icons.id} style={[styles.iconItem]} >    
                  <TouchableOpacity onPress={() => iconSelected(icons) }>
                    <View style={styles.iconInner}>
                        {/* <Image style={styles.tileImage} source={icons.icon} /> */}
                        <Image source={{uri: ''+icons.icon+''}} style={styles.tileImage} />
                        <Text style={styles.tileText}>{icons.name}</Text>
                    </View>
                  </TouchableOpacity>
              </View>
            ))
          : 
          <View></View>
          )
        
        }
      </View>
    );
  }

  useEffect(() => {
    
    loadIconsHere();
    // checkIfUserHasLogin();
    // checkPermission();
  }, []);

  return (
    
    <ScrollView style={styles.mainComponent}>
      <View>
          
          <View style={styles.header}>
            <View style={styles.mainColHead}>
              <View style={styles.mainColLeft}>
                <Text style={styles.hello_header}>Tiles for</Text>
                <Text style={styles.user_name}>{navigation.getParam('name')}</Text>
              </View>
              <View style={styles.mainColRight}>
                {/* <TouchableOpacity onPressIn={() => navigation.navigate('Profile')}>
                  <Image style={styles.isUserImage} source={require('./../assets/images/user.png')} />
                </TouchableOpacity> */}
              </View>
            </View>
          </View>
          {/* <Text>{ navigation.getParam('icons')}</Text> */}
          {loadIcons()}
          {/* <Button title="Go Back" style={styles.backButton} onPress={() => navigation.goBack() } /> */}
          <Text style={styles.backButton} onPress={() => navigation.goBack() }>Go Back</Text>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  header: {
    width: '100%',
    height: 80,
    padding: 20,
    marginTop: 60,
    justifyContent: 'center',
  },
  mainColHead: {
    flexDirection: "row",
    flexWrap: 'wrap',
  },
  mainColLeft: {
    width: '77%'
  },
  mainColRight: {
    width: '20%',
  },
  hello_header: {
    fontSize: 18,
    color: "#fff",
    fontFamily: 'PTSansNarrow-Bold',
  },
  user_name: {
    fontSize: 26,
    fontWeight: '600',
    color: '#fff',
    fontFamily: 'PTSansNarrow-Bold',
  },
  isUserImage: {
    width: 39,
    height: 39,
    // position: 'absolute',
    // right: 0
  },
  backButton:{
    backgroundColor: '#e9e367',
    fontFamily: 'PTSansNarrow-Bold',
    fontSize: 16,
    textAlign: 'center',
    paddingVertical: 10,
    borderRadius: 5,
    marginHorizontal: '30%',
    marginTop: 40,
    marginBottom: 40
  },
  optionsFor:{
    fontFamily: 'PTSansNarrow-Bold',
    fontSize: 18,
    color: '#fff'
  },
  actualTiles: {
    fontFamily: 'PTSansNarrow-Bold',
    fontSize: 28,
    color: '#fff'
  },  
  mainComponent: {
    flex: 1,
    backgroundColor: '#2b2b2b'
  },
  iconItemRow: {
    flexDirection: "row",
    flexWrap: 'wrap',
    width: '92%',
    margin: '4%',
    // padding: 20,
    // backgroundColor: "red",
    flex: 1
  },
  iconItem: {
    // flex: 1,
    width: '33%',
    height: 120,
    padding: 5,
    marginBottom: 30
  },
  iconInner: {
    // flex: 1,
    height: 120,
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: 'pink'
  },
  tileImage: {
    marginHorizontal: '5%',
    borderRadius: 5,
    borderWidth: 6,
    borderColor: '#e9e367',
    width: 100,
    height: 100,
  },
  tileText: {
    color: '#fff',
    fontFamily: 'PTSansNarrow-Regular',
    paddingTop: 10,
    fontSize: 16
  }
});

const mapStateToProps = (state, wordProps) => {
  return {
    words: state.Sentenses.word
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addWord: (word) => dispatch(addWord(word)),
    resetWord: () => dispatch(resetWord())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Tiles);
