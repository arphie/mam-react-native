/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect} from 'react';

import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  useColorScheme,
  View,
  Platform
} from 'react-native';

import Navigation from './routes/stackNavi'

import Dashboard from './screens/Dashboard'

// redux
import { Provider } from 'react-redux';
import Store from './redux/Store';


const App = () => {

  


  return (
      <Provider store={Store}>
        <Navigation />
      </Provider>
  );

}

const styles = StyleSheet.create({
  mainFrame: {
    // backgroundColor: "#eaeef4",
    flex: 1,
    flexGrow: 1
  },
});

export default App;
